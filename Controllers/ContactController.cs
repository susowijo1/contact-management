﻿using ContactManagement.DAL.Comments;
using ContactManagement.DAL.Contacts;
using ContactManagement.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ContactManagement.Controllers
{
    public class ContactController : Controller
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IContactRepository _contactRepository;
        private readonly string _errorMsg = "L'opération a échoué, veuillez consulter les raisons sur votre page !";

        public ContactController()
        {
            _commentRepository = new CommentRepository();
            _contactRepository = new ContactRepository();
        }

        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        // GET: Contact
        public ActionResult List(int page = 1, ModeType mode = ModeType.List, string searchText = null)
        {
            // configure paging
            var pageIndex = page;
            int perPage = 10;
            int skip = (pageIndex - 1) * perPage;

            var tupleContacts = new Tuple<int, ICollection<Contact>>(0, new List<Contact>());
            try
            {
                // get data
                if (mode == ModeType.List)
                    tupleContacts = _contactRepository.GetAll(skip, perPage);
                else // search by name
                    tupleContacts = _contactRepository.GetByName(searchText, skip, perPage);
            }
            catch (Exception ex)
            {
                // show error to view
                ModelState.AddModelError("error", ex.InnerException?.Message ?? ex.Message);
                // save notification message temporaly
                TempData["Error"] = ex.InnerException?.Message ?? ex.Message;
            }
            // get total rows exists in database
            var totalContacts = tupleContacts.Item1;
            var totalPage = Math.Ceiling((double)totalContacts / (double)perPage);

            var contacts = tupleContacts.Item2;
            ViewBag.TotalPage = totalPage;
            ViewBag.TotalResults = tupleContacts.Item1;
            // specify if we are in 'Search mode' or 'List mode'
            ViewBag.Mode = mode;
            // back text to view
            ViewBag.SearchText = searchText;


            return View(contacts);
        }

        // POST: Contact/Search
        public ActionResult Search(string search, int page = 1)
        {
            // if user press Enter keyword, without write any text in the search input
            if (string.IsNullOrEmpty(search))
            {
                ModelState.AddModelError("error", "Veuillez renseigner l'information a rechercher !");
                // save notification message temporaly
                TempData["Error"] = "Veuillez renseigner l'information a rechercher !";
                return RedirectToAction("List");
            }
            return RedirectToAction("List", new { page = page, searchText = search, mode = ModeType.Search });
        }


        // GET: Contact/Details/5
        public ActionResult Details(long id)
        {
            try
            {
                var foundContact = _contactRepository.Get(id);
                if (foundContact == null)
                {
                    // back to list with error msg
                    ModelState.AddModelError("error", "Contact introuvable !");
                    // save notification message temporaly
                    TempData["Error"] = "Contact introuvable !";
                    return RedirectToAction("List");
                }
                // get all contact comment's
                var comments = _commentRepository.GetAllByContact(foundContact.Id.Value);
                // use to show all comments inside the view
                ViewBag.Comments = comments.Item2.Select(x => x.Content);

                return View(foundContact);
            }
            catch (Exception ex)
            {
                // show error to view
                ModelState.AddModelError("error", ex.InnerException?.Message ?? ex.Message);
                // save notification message temporaly
                TempData["Error"] = ex.InnerException?.Message ?? ex.Message;
                return RedirectToAction("List");
            }
        }

        // GET: Contact/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contact/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Contact contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //extract all form cmments
                    var commentVals = Request.Form.GetValues("comment");
                    // constuct comment entity model list: (the contactId value will be replaced
                    // in all comments after the contact is created)
                    List<Comment> comments = commentVals?
                        .Select(x => new Comment(x, 0)).ToList() ?? new List<Comment>();

                    // set contacts comments
                    contact.Comments = comments;

                    _contactRepository.Create(contact);

                    // save notification message temporaly
                    TempData["Success"] = "Le contact a été ajouté avec success !";
                    return RedirectToAction("List");
                }
                catch(Exception ex)
                {
                    // show error to view
                    ModelState.AddModelError("error", ex.InnerException?.Message ?? ex.Message);
                    // save notification message temporaly
                    TempData["Error"] = ex.InnerException?.Message ?? ex.Message;
                    return View();
                }
            }
            return View();
        }

        // GET: Contact/Edit/5
        public ActionResult Edit(long id)
        {
            try
            {
                var foundContact = _contactRepository.Get(id);
                if (foundContact == null)
                {
                    // back to list with error msg
                    ModelState.AddModelError("error", "Contact introuvable !");
                    // save notification message temporaly
                    TempData["Error"] = "Contact introuvable !";
                    return RedirectToAction("List");
                }
                // get all contact comment's
                var comments = _commentRepository.GetAllByContact(foundContact.Id.Value);

                // use to show all comments inside the view
                ViewBag.Comments = comments.Item2.Select(x => x.Content);

                return View(foundContact);
            }
            catch (Exception ex)
            {
                // show error to view
                ModelState.AddModelError("error", ex.InnerException?.Message ?? ex.Message);
                // save notification message temporaly
                TempData["Error"] = ex.InnerException?.Message ?? ex.Message;
                return RedirectToAction("List");
            }
        }

        // POST: Contact/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(long id, Contact contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //extract all form cmments
                    var commentVals = Request.Form.GetValues("comment");
                    // constuct comment entity model list
                    List<Comment> comments = commentVals?
                        .Select(x => new Comment(x, id)).ToList() ?? new List<Comment>();

                    // update contacts comments
                    contact.Comments = comments;

                    _contactRepository.Update(id, contact);

                    // save notification message temporaly
                    TempData["Success"] = "Le contact a été mis a jour !";
                    return RedirectToAction("List");
                }
                catch (Exception ex)
                {
                    // reload model data
                    var foundContact = _contactRepository.Get(id);
                    if (foundContact == null)
                    {
                        // back to list with error msg
                        ModelState.AddModelError("error", "Contact introuvable !");
                        // save notification message temporaly
                        TempData["Error"] = "Contact introuvable !";
                        return RedirectToAction("List");
                    }
                    // get all contact comment's
                    var comments = _commentRepository.GetAllByContact(foundContact.Id.Value);

                    // use to show all comments inside the view
                    ViewBag.Comments = comments.Item2.Select(x => x.Content);

                    // show error to view
                    ModelState.AddModelError("error", ex.InnerException?.Message ?? ex.Message);
                    // save notification message temporaly
                    TempData["Error"] = ex.InnerException?.Message ?? ex.Message;
                    return View(foundContact);
                }
            }

            // reload model data
            var foundContact_ = _contactRepository.Get(id);
            if (foundContact_ == null)
            {
                // back to list with error msg
                ModelState.AddModelError("error", "Contact introuvable !");
                // save notification message temporaly
                TempData["Error"] = "Contact introuvable !";
                return RedirectToAction("List");
            }
            // get all contact comment's
            var comments_ = _commentRepository.GetAllByContact(foundContact_.Id.Value);

            // use to show all comments inside the view
            ViewBag.Comments = comments_.Item2.Select(x => x.Content);

            return View(foundContact_);
        }

        // POST: Contact/Delete/5
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                _contactRepository.Delete(id);

                // save notification message temporaly
                TempData["Success"] = "Le contact a été correctement supprimé !";
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                // show error to view
                ModelState.AddModelError("error", ex.InnerException?.Message ?? ex.Message);
                // save notification message temporaly
                TempData["Error"] = ex.InnerException?.Message ?? ex.Message;
                return RedirectToAction("List");
            }
        }

        // GET: Contact/Import
        public ActionResult Import()
        {
            return View();
        }

        // POST: Contact/Import
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase fileUpload)
        {
            if(fileUpload == null)
            {
                // show error to view
                ModelState.AddModelError("error", "Veuillez sélectionner un fichier valide !");
                // save notification message temporaly
                TempData["Error"] = "Veuillez sélectionner un fichier valide !";
                return View();
            }
            try
            {
                _contactRepository.ImportContact(fileUpload);

                // save notification message temporaly
                TempData["Success"] = "Les contacts ont été importer avec success !";
                return RedirectToAction("List");
            }
            catch(Exception ex)
            {
                // show error to view
                ModelState.AddModelError("error", ex.InnerException?.Message ?? ex.Message);
                
                // save notification message temporaly
                TempData["Error"] = ex.InnerException?.Message ?? ex.Message;
                return View();
            }
        }
    }
}
