﻿using ContactManagement.Models;
using System;
using System.Collections.Generic;
using System.Web;

namespace ContactManagement.DAL.Contacts
{
    /// <summary>
    /// List the methods to define in ContentRepository
    /// </summary>
    public interface IContactRepository
    {
        /// <summary>
        /// This method save the contact
        /// </summary>
        /// <param name="contact">Represents the contact data</param>
        /// <returns>Contact</returns>
        void Create(Contact contact);

        /// <summary>
        /// This method save many contacts
        /// </summary>
        /// <param name="contacts">Represents the contacts data</param>
        /// <returns></returns>
        void CreateMany(ICollection<Contact> contacts);

        /// <summary>
        /// This method update the contact
        /// </summary>
        /// <param name="contact">Represents the contact data</param>
        /// <param name="id">Represents the id (in databse) of the contact</param>
        /// <returns></returns>
        void Update(long id, Contact contact);

        /// <summary>
        /// This method delete contact that have the specified id as a parameter
        /// </summary>
        /// <param name="id">Represents the id of the contact</param>
        /// <returns></returns>
        void Delete(long id);

        /// <summary>
        /// This method gets all contacts that her name content the specified text as a parameter
        /// </summary>
        /// <param name="name">Represents the text we are looking for in the lastname
        /// or first name</param>
        /// <param name="skip">The number of contacts to skip</param>
        /// <param name="take">The number of contacts to take</param>
        /// <returns>Tuple<int, ICollection<Contact>></returns>
        Tuple<int, ICollection<Contact>> GetByName(string name, int? skip = null,
            int? take = null);

        /// <summary>
        /// This method gets all contacts
        /// </summary>
        /// <param name="skip">The number of contacts to skip</param>
        /// <param name="take">The number of contacts to take</param>
        /// <returns>Tuple<int, ICollection<Contact>></returns>
        Tuple<int, ICollection<Contact>> GetAll(int? skip = null,
            int? take = null);

        /// <summary>
        /// This method get the contact that have the specified email as a parameter
        /// </summary>
        /// <param name="email">Represents the email of the contact</param>
        /// <returns>Contact</returns>
        Contact GetByEmail(string email);

        /// <summary>
        /// This method get the contact that have the specified id as a parameter
        /// </summary>
        /// <param name="id">Represents the id of the contact</param>
        /// <returns>Contact</returns>
        Contact Get(long id);

        /// <summary>
        /// This method imports contacts from an excel file and saves them
        /// </summary>
        /// <param name="file">Represents the file .csv</param>
        /// <returns></returns>
        void ImportContact(HttpPostedFileBase file);
    }
}
