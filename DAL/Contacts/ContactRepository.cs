﻿using ContactManagement.DAL.Comments;
using ContactManagement.Models;
using Microsoft.Ajax.Utilities;
using MySql.Data.MySqlClient;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace ContactManagement.DAL.Contacts
{
    /// <summary>
    /// class ContactRepository: used for contact management
    /// ( defined the methods listed in IContentRepository)
    /// </summary>
    public class ContactRepository : IContactRepository
    {
        #region Properties (Private)
        private readonly string constr;
        private readonly ICommentRepository _commentRepository;
        #endregion

        #region Constructor
        public ContactRepository()
        {
            constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            _commentRepository = new CommentRepository();
        }
        #endregion

        #region Methods (Public)

        public void Create(Contact contact)
        {
            int nbrRowAffected = -1;

            // check if email already exists
            var foundContact = GetByEmail(contact.Email);
            if (foundContact != null)
                throw new Exception($"L'email '{contact.Email}' existe déja !");

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_insert_contact", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_firstname", contact.FirstName);
                cmd.Parameters.AddWithValue("_lastname", contact.LastName);
                cmd.Parameters.AddWithValue("_email", contact.Email);
                cmd.Parameters.AddWithValue("_telephone", contact.Telephone);
                cmd.Parameters.AddWithValue("_city", contact.City);
                cmd.Parameters.AddWithValue("_province", contact.Province);
                cmd.Parameters.AddWithValue("_postalcode", contact.PostalCode.ToUpper());
                cmd.Parameters.AddWithValue("_country", contact.Country);

                con.Open();
                // execute query and retrieve number row affected
                nbrRowAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            // throw an exception if the save failed
            if (nbrRowAffected == -1)
                throw new Exception($"Une erreur est survenu lors de l'enregistrement du contact:  '{contact.Email}' !");

            // get the id of the last contact
            int currentContactId = GetContactLastId();

            // delete all contact comments
            _commentRepository.DeleteManyByContact(currentContactId);

            // save contact comments if exists
            if (contact.Comments?.Any() ?? false) 
                _commentRepository.CreateMany(contact.Comments.ToList(), currentContactId);
        }

        public void CreateMany(ICollection<Contact> contacts)
        {
            foreach (Contact contact in contacts)
            {
                try
                {
                    // create contact
                    Create(contact);
                }
                catch (Exception ex)
                {
                    // check if the exception specifies that the email already exists
                    if (ex.Message.Contains(contact.Email))
                    {
                        var contactFound = GetByEmail(contact.Email);
                        // update existing contact
                        Update(contactFound.Id.Value, contact);
                    }
                    else // throw again same exception
                        throw ex;
                }
            }
        }

        public void Update(long id, Contact contact)
        {
            int nbrRowAffected = -1;

            // check if id exists
            var foundContactById = Get(id);
            if (foundContactById == null)
                throw new Exception("Contact introuvable !");

            // check if email have change
            if (contact.Email != foundContactById.Email)
            {
                // check if email already exists
                var foundContactByEmail = GetByEmail(contact.Email);
                if (foundContactByEmail != null)
                    throw new Exception($"L'email '{contact.Email}' existe déja !");
            }

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_update_contact", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_id", id);
                cmd.Parameters.AddWithValue("_updated_on", DateTime.Now);
                cmd.Parameters.AddWithValue("_firstname", contact.FirstName);
                cmd.Parameters.AddWithValue("_lastname", contact.LastName);
                cmd.Parameters.AddWithValue("_email", contact.Email);
                cmd.Parameters.AddWithValue("_telephone", contact.Telephone);
                cmd.Parameters.AddWithValue("_city", contact.City);
                cmd.Parameters.AddWithValue("_province", contact.Province);
                cmd.Parameters.AddWithValue("_postalcode", contact.PostalCode);
                cmd.Parameters.AddWithValue("_country", contact.Country);

                con.Open();
                // execute query and retrieve number row affected
                nbrRowAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            if(nbrRowAffected == -1)
                throw new Exception($"Une erreur est survenu lors de la mise a jour du contact: '{contact.Email}'");

            // delete all contact comments
            _commentRepository.DeleteManyByContact(id);

            // save contact comments if exists
            if (contact.Comments?.Any() ?? false)
                _commentRepository.CreateMany(contact.Comments.ToList(), id);
        }

        public void Delete(long id)
        {
            int nbrRowAffected = -1;
            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_delete_contact", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_id", id);

                con.Open();
                // execute query and retrieve number row affected
                nbrRowAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            if (nbrRowAffected == -1)
                throw new Exception($"Contact introuvable !");
        }

        public Tuple<int, ICollection<Contact>> GetByName(string name, int? skip = null,
            int? take = null)
        {
            List<Contact> contacts = new List<Contact>();
            int totalRows = 0;

            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_get_contacts_by_name", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_name", name);
                cmd.Parameters.AddWithValue("_offset", skip ?? 0);
                cmd.Parameters.AddWithValue("_count", take ?? 999999);
                cmd.Connection = con;
                con.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                // set data to model if rows exists
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var contact = new Contact
                        {
                            Id = reader.GetInt64(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            Email = reader.GetString(3),
                            Telephone = reader.GetString(4),
                            City = reader.GetString(5),
                            Province = reader.GetString(6),
                            PostalCode = reader.GetString(7),
                            Country = reader.GetString(8),
                            CreatedOn = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9),
                            UpdatedOn = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10)
                        };
                        contacts.Add(contact);
                    }
                    // read another table (retrieve 'total' field)
                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            totalRows = reader.GetInt32(0);
                        }
                    }
                    //close reader
                    reader.Close();
                }
                // close connection
                con.Close();
            }
            // set total comments for each contact
            contacts = contacts.Select(x =>
            {
                x.TotalComment = _commentRepository.GetCountByContact(x.Id.Value);
                return x;
            }).ToList();

            return new Tuple<int, ICollection<Contact>>(totalRows, contacts);
        }

        public Tuple<int, ICollection<Contact>> GetAll(int? skip = null,
            int? take = null)
        {
            List<Contact> contacts = new List<Contact>();
            int totalRows = 0;

            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_get_contacts", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_offset", skip ?? 0);
                cmd.Parameters.AddWithValue("_count", take ?? 999999);
                cmd.Connection = con;
                con.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                // set data to model if rows exists
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var contact = new Contact
                        {
                            Id = reader.GetInt64(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            Email = reader.GetString(3),
                            Telephone = reader.GetString(4),
                            City = reader.GetString(5),
                            Province = reader.GetString(6),
                            PostalCode = reader.GetString(7),
                            Country = reader.GetString(8),
                            CreatedOn = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9),
                            UpdatedOn = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10)
                        };
                        contacts.Add(contact);
                    }
                    // read another table (retrieve 'total' field)
                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            totalRows = reader.GetInt32(0);
                        }
                    }
                    //close reader
                    reader.Close();
                }
                // close connection
                con.Close();
            }
            // set total comment by contact
            contacts = contacts.Select(x =>
            {
                x.TotalComment = _commentRepository.GetCountByContact(x.Id.Value);
                return x;
            }).ToList();

            return new Tuple<int, ICollection<Contact>>(totalRows, contacts);
        }

        public Contact GetByEmail(string email)
        {
            Contact contact = null; ;

            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_get_contact_by_email", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_email", email);
                cmd.Connection = con;
                con.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                // set data to model if rows exists
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        contact = new Contact
                        {
                            Id = reader.GetInt64(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            Email = reader.GetString(3),
                            Telephone = reader.GetString(4),
                            City = reader.GetString(5),
                            Province = reader.GetString(6),
                            PostalCode = reader.GetString(7),
                            Country = reader.GetString(8),
                            CreatedOn = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9),
                            UpdatedOn = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10)
                        };
                    }
                    //close reader
                    reader.Close();
                }
                // close connection
                con.Close();
            }

            return contact;
        }

        public Contact Get(long id)
        {
            Contact contact = null; ;

            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_get_contact", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_id", id);
                cmd.Connection = con;
                con.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                // set data to model if rows exists
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        contact = new Contact
                        {
                            Id = reader.GetInt64(0),
                            FirstName = reader.GetString(1),
                            LastName = reader.GetString(2),
                            Email = reader.GetString(3),
                            Telephone = reader.GetString(4),
                            City = reader.GetString(5),
                            Province = reader.GetString(6),
                            PostalCode = reader.GetString(7),
                            Country = reader.GetString(8),
                            CreatedOn = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9),
                            UpdatedOn = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10)
                        };
                    }
                    //close reader
                    reader.Close();
                }
                // close connection
                con.Close();
            }

            return contact;
        }

        public void ImportContact(HttpPostedFileBase file)
        {
            var fileExt = Path.GetExtension(file.FileName).Substring(1).ToLower();
            //check file type
            if (fileExt != "csv")
                throw new Exception($"Seule les fichiers au format CSV sont autorisé !");

            var contacts = new List<Contact>();
            //var path = HttpContext.Current.Server.MapPath(@"~/App_Data/data.csv");
            using (StreamReader sr = new StreamReader(file.InputStream))
            {
                string line;
                string[] columns = null;
                int lineIndex = 0;
                // retrieve each line
                while ((line = sr.ReadLine()) != null)
                {
                    lineIndex++;
                    if(lineIndex != 1)
                    {
                        // get array from line
                        columns = line.Split(',');

                        // throw an exception if the number of cells for the contact does not
                        // correspond to the number of fields of the model
                        if (columns.Length < 8)
                            throw new Exception("Nombre de colonne insuffisant, vous devez en avoir au moins 8 " +
                                $":  ~~(Ligne {lineIndex})");

                        //fill contact data
                        var contact = new Contact(columns[0], columns[1], columns[2], columns[3],
                                columns[4], columns[5], columns[6], columns[7]);

                        // validate contact, and throw exception if contains errors
                        var validationErrors = ValidateContact(contact);
                        if (validationErrors.Any())
                            throw new Exception($"Le contact n'est pas valide a la (ligne {lineIndex}) => " + 
                                string.Join("; ", validationErrors));

                        // fill comment if exists
                        if (columns.Length > 8)
                        {
                            contact.Comments = new List<Comment>();
                            for (int i = 8; i < columns.Length; i++)
                            {
                                if(!string.IsNullOrEmpty(columns[i]))
                                {
                                    var comment = new Comment() { Content = columns[i] };
                                    contact.Comments.Add(comment);
                                }
                            }
                        }
                        contacts.Add(contact);
                    }
                }
            }

            // save contacts
            CreateMany(contacts);
        }

        #endregion

        #region Methods (Private)

        /// <summary>
        /// This method validate the contact
        /// </summary>
        /// <param name="contact">Represents the contact to validate</param>
        /// <returns>List of validation errors</returns>
        private ICollection<string> ValidateContact(Contact contact)
        {
            var context = new ValidationContext(contact, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();
            var errorMessages = new List<string>();

            var v = Validator.TryValidateObject(contact, context, validationResults, true);
            if (validationResults.Any())
                errorMessages = validationResults.Select(x => x.ErrorMessage).ToList();

            return errorMessages;
        }

        /// <summary>
        /// Get the Id of the last contact inserted
        /// </summary>
        /// <returns>Last Id</returns>
        private int GetContactLastId()
        {
            int maxId = 0;
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                var query = "SELECT MAX(id) FROM contact";
                var cmd = new MySqlCommand(query, con);

                con.Open();
                // execute query and retrieve number row affected
                MySqlDataReader reader = cmd.ExecuteReader();

                // set data to model if rows exists
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        maxId = reader.GetInt32(0);
                    }
                    //close reader
                    reader.Close();
                }
                // close connection
                con.Close();
            }
            return maxId;
        }

        #endregion
    }
}