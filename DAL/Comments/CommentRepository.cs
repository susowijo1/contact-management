﻿using ContactManagement.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.Helpers;
using System.Web.UI.WebControls;

namespace ContactManagement.DAL.Comments
{
    /// <summary>
    /// class CommentRepository: used for comment management
    /// ( defined the methods listed in ICommentRepository)
    /// </summary>
    public class CommentRepository : ICommentRepository
    {
        #region Properties (Private)
        private readonly string constr;
        #endregion

        #region Constructor
        public CommentRepository()
        {
            constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        }
        #endregion

        #region Methods (Public)
        
        public void Create(Comment comment)
        {
            int nbrRowAffected = -1;
            using (MySqlConnection con = new MySqlConnection(constr)) 
            { 
                var cmd = new MySqlCommand("sp_insert_comment", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_content", comment.Content);
                cmd.Parameters.AddWithValue("_email", comment.ContactId);

                con.Open();
                // execute query and retrieve number row affected
                nbrRowAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            // throw an exception if the save failed
            if (nbrRowAffected == -1)
                throw new Exception($"Une erreur est survenu lors de l'enregistrement du commentaire !");  
        }

        public void CreateMany(ICollection<Comment> comments, long contactId)
        {
            int nbrRowAffected = -1;
            using (MySqlConnection con = new MySqlConnection(constr)) 
            {
                var query = "INSERT INTO comment (content, contact_id) VALUES ";

                // complete query
                for (int i = 1; i <= comments.Count; i++)
                    query += $"(@content{i}, @contact_id{i}),";
                // remove the last "," in query sting
                query = query.Remove(query.Length - 1, 1);

                var cmd = new MySqlCommand(query, con);

                // set procedure parameters
                var commentList = comments.ToList();
                for (int i = 1; i <= comments.Count; i++)
                {
                    cmd.Parameters.AddWithValue($"@content{i}", commentList[i-1].Content);
                    cmd.Parameters.AddWithValue($"@contact_id{i}", contactId);
                }

                con.Open();
                // execute query and retrieve number row affected
                nbrRowAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            // throw an exception if the save failed
            if (nbrRowAffected == -1)
                throw new Exception("Une erreur est survenu lors de l'enregistrement de certains commentaires !");
        }

        public void Update(long id, Comment comment)
        {
            int nbrRowAffected = -1;
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_update_comment", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_content", comment.Content);
                cmd.Parameters.AddWithValue("_updated_on", DateTime.Now);
                cmd.Parameters.AddWithValue("_id", id);

                con.Open();
                // execute query and retrieve number row affected
                cmd.ExecuteNonQuery();
                con.Close();
            }
            if (nbrRowAffected == -1)
                throw new Exception("Une erreur est survenu lors de la mise a jour du commentaire !");
        }

        public void Delete(long id)
        {
            int nbrRowAffected = -1;
            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_delete_comment", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_id", id);

                con.Open();
                // execute query and retrieve number row affected
                nbrRowAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            if (nbrRowAffected == -1)
                throw new Exception($"Commentaire introuvable !");
        }

        public void DeleteManyByContact(long contactId)
        {
            int nbrRowAffected = -1;
            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_delete_comments_by_contact_id", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_contact_id", contactId);

                con.Open();
                // execute query and retrieve number row affected
                nbrRowAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            if (nbrRowAffected == -1)
                throw new Exception($"Certains commentaires sont introuvables !");
        }

        public Tuple<int, ICollection<Comment>> GetAllByContact(long contactId, int? skip = null,
            int? take = null)
        {
            List<Comment> comments = new List<Comment>();
            int totalRows = 0;

            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_get_comments_by_contact_id", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_contact_id", contactId);
                cmd.Parameters.AddWithValue("_offset", skip ?? 0);
                cmd.Parameters.AddWithValue("_count", take ?? 999999);
                cmd.Connection = con;
                con.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                // set data to model if rows exists
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var comment = new Comment
                        {
                            Id = reader.GetInt32(0),
                            ContactId = reader.GetInt64(1),
                            Content = reader.GetString(2),
                            CreatedOn = reader.GetDateTime(3),
                            UpdatedOn = reader.IsDBNull(4) ? (DateTime?)null : reader.GetDateTime(4),
                        };
                        comments.Add(comment);
                    }
                    // read another table (retrieve 'total' field)
                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            totalRows = reader.GetInt32(0);
                        }
                    }
                    //close reader
                    reader.Close();
                }
                // close connection
                con.Close();
            }
            return new Tuple<int, ICollection<Comment>>(totalRows, comments);
        }

        public Comment Get(long id)
        {
            Comment comment = null; ;

            using (var con = new MySqlConnection(constr))
            {
                var cmd = new MySqlCommand("sp_get_comment", con);

                cmd.CommandType = CommandType.StoredProcedure;
                // set procedure parameters
                cmd.Parameters.AddWithValue("_id", id);
                cmd.Connection = con;
                con.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                // set data to model if rows exists
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        comment = new Comment
                        {
                            Id = reader.GetInt32(0),
                            ContactId = reader.GetInt64(1),
                            Content = reader.GetString(2),
                            CreatedOn = reader.GetDateTime(3),
                            UpdatedOn = reader.IsDBNull(4) ? (DateTime?)null : reader.GetDateTime(4),
                        };
                    }
                    //close reader
                    reader.Close();
                }
                // close connection
                con.Close();
            }

            return comment;
        }

        public int GetCountByContact(long contactId)
        {
            int count = 0;
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                var query = "SELECT COUNT(*) FROM comment WHERE contact_id = @contact_id";
                var cmd = new MySqlCommand(query, con);

                // set procedure parameters
                cmd.Parameters.AddWithValue("@contact_id", contactId);

                con.Open();
                // execute query and retrieve number row affected
                MySqlDataReader reader = cmd.ExecuteReader();

                // set data to model if rows exists
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        count = reader.GetInt32(0);
                    }
                    //close reader
                    reader.Close();
                }
                // close connection
                con.Close();
            }
            return count;
        }
        #endregion

        #region Methods (Private)


        #endregion
    }
}