﻿using ContactManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.DAL.Comments
{
    /// <summary>
    /// List the methods to define in CommentRepository
    /// </summary>
    public interface ICommentRepository
    {
        /// <summary>
        /// This method save the comment
        /// </summary>
        /// <param name="comment">Represents the comment data</param>
        /// <returns></returns>
        void Create(Comment comment);

        /// <summary>
        /// This method save many comments
        /// </summary>
        /// <param name="comments">Represents the comments data</param>
        /// <param name="contactId">Represents the id of the contact concerned by these comments</param>
        /// <returns></returns>
        void CreateMany(ICollection<Comment> comments, long contactId);

        /// <summary>
        /// This method update the comment
        /// </summary>
        /// <param name="comment">Represents the comment data</param>
        /// <param name="id">Represents the id of the comment</param>
        /// <returns></returns>
        void Update(long id, Comment comment);

        /// <summary>
        /// This method delete comment that have the specified id as a parameter
        /// </summary>
        /// <param name="id">Represents the id of the comment</param>
        /// <returns></returns>
        void Delete(long id);

        /// <summary>
        /// This method delete all comments that have the specified contactId as a parameter
        /// </summary>
        /// <param name="contactId">Represents the if of the contact</param>
        /// <returns></returns>
        void DeleteManyByContact(long contactId);

        /// <summary>
        /// This method count all comments that have the specified contactId as a parameter
        /// </summary>
        /// <param name="contactId">Represents the contactId of the comment</param>
        /// <returns>Total count</returns>
        int GetCountByContact(long contactId);

        /// <summary>
        /// This method gets all comments that have the specified email as a parameter
        /// </summary>
        /// <param name="email">Represents the email of the comment</param>
        /// <param name="skip">The number of comments to skip</param>
        /// <param name="take">The number of comments to take</param>
        /// <returns>Tuple<int, ICollection<Comment>></returns>
        Tuple<int, ICollection<Comment>> GetAllByContact(long contactId, int? skip = null,
            int? take = null);

        /// <summary>
        /// This method get the comment that have the specified id as a parameter
        /// </summary>
        /// <param name="id">Represents the id of the comment</param>
        /// <returns>Comment</returns>
        Comment Get(long id);
    }
}
