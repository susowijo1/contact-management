-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le : Dim 09 oct. 2022 à 14:31
-- Version du serveur :  5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données : `contact_manag_db`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `sp_delete_comment`$$
CREATE PROCEDURE `sp_delete_comment` (IN `_id` INT)  NO SQL
DELETE FROM comment WHERE id = _id$$

DROP PROCEDURE IF EXISTS `sp_delete_comments_by_contact_id`$$
CREATE PROCEDURE `sp_delete_comments_by_contact_id` (IN `_contact_id` INT)  NO SQL
DELETE FROM comment WHERE contact_id = _contact_id$$

DROP PROCEDURE IF EXISTS `sp_delete_contact`$$
CREATE PROCEDURE `sp_delete_contact` (IN `_id` INT(50))  NO SQL
DELETE FROM contact WHERE id = _id$$

DROP PROCEDURE IF EXISTS `sp_get_comment`$$
CREATE PROCEDURE `sp_get_comment` (IN `_id` INT)  NO SQL
SELECT * FROM comment WHERE email = _id$$

DROP PROCEDURE IF EXISTS `sp_get_comments_by_contact_id`$$
CREATE PROCEDURE `sp_get_comments_by_contact_id` (IN `_contact_id` INT, IN `_offset` INT, IN `_count` INT)  NO SQL
BEGIN
	SELECT SQL_CALC_FOUND_ROWS * 
	FROM comment 
	WHERE contact_id = _contact_id
	LIMIT _offset, _count;
    
	SELECT FOUND_ROWS() as total;
END$$

DROP PROCEDURE IF EXISTS `sp_get_contact`$$
CREATE PROCEDURE `sp_get_contact` (IN `_id` INT)  NO SQL
SELECT * FROM contact WHERE id = _id$$

DROP PROCEDURE IF EXISTS `sp_get_contacts`$$
CREATE PROCEDURE `sp_get_contacts` (IN `_offset` INT, IN `_count` INT)  BEGIN
   	SELECT SQL_CALC_FOUND_ROWS *
   	FROM contact
   	LIMIT _offset, _count;
    
	SELECT FOUND_ROWS() as total;
END$$

DROP PROCEDURE IF EXISTS `sp_get_contacts_by_name`$$
CREATE PROCEDURE `sp_get_contacts_by_name` (IN `_name` VARCHAR(50) CHARSET utf8, IN `_offset` INT, IN `_count` INT)  BEGIN
	SELECT SQL_CALC_FOUND_ROWS *
    FROM contact
    WHERE firstname LIKE CONCAT('%', _name, '%') OR lastname LIKE CONCAT('%', _name, '%')
    LIMIT _offset, _count;
    
    SELECT FOUND_ROWS() as total;
END$$

DROP PROCEDURE IF EXISTS `sp_get_contact_by_email`$$
CREATE PROCEDURE `sp_get_contact_by_email` (IN `_email` VARCHAR(50) CHARSET utf8)  NO SQL
SELECT * FROM contact WHERE email = _email$$

DROP PROCEDURE IF EXISTS `sp_insert_comment`$$
CREATE PROCEDURE `sp_insert_comment` (IN `_content` TEXT, IN `_email` VARCHAR(50))  NO SQL
INSERT INTO comment (content, email) VALUES(_content, _email)$$

DROP PROCEDURE IF EXISTS `sp_insert_contact`$$
CREATE PROCEDURE `sp_insert_contact` (IN `_firstname` VARCHAR(50), IN `_lastname` VARCHAR(50), IN `_email` VARCHAR(50), IN `_telephone` VARCHAR(50), IN `_city` VARCHAR(50), IN `_province` VARCHAR(50), IN `_postalcode` VARCHAR(50), IN `_country` VARCHAR(50))  NO SQL
INSERT INTO contact (firstname, lastname, email, telephone, city, province, postalcode, country) 
VALUES(_firstname, _lastname, _email, _telephone, _city, _province, _postalcode, _country)$$

DROP PROCEDURE IF EXISTS `sp_update_comment`$$
CREATE PROCEDURE `sp_update_comment` (IN `_content` TEXT, IN `_id` INT, IN `_updated_on` DATETIME)  NO SQL
UPDATE comment SET content = _content, updated_on = _updated_on 
WHERE id = _id$$

DROP PROCEDURE IF EXISTS `sp_update_contact`$$
CREATE PROCEDURE `sp_update_contact` (IN `_id` INT, IN `_firstname` VARCHAR(50), IN `_lastname` VARCHAR(50), IN `_email` VARCHAR(50), IN `_telephone` VARCHAR(50), IN `_city` VARCHAR(50), IN `_province` VARCHAR(50), IN `_postalcode` VARCHAR(50), IN `_country` VARCHAR(50), IN `_updated_on` DATETIME)  NO SQL
UPDATE contact SET firstname = _firstname, lastname = _lastname, email = _email, telephone = _telephone, city = _city, province = _province, postalcode = _postalcode, country = _country, updated_on = _updated_on
WHERE id=_id$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_id` bigint(20) NOT NULL,
  `content` text NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_contact` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `contact_id`, `content`, `created_on`, `updated_on`) VALUES
(6, 6, 'Mon premier comment', '2022-10-08 05:28:35', NULL),
(7, 6, 'Je valide la ville mon ami', '2022-10-08 05:28:35', NULL),
(8, 7, 'Mon premier comment', '2022-10-08 05:32:59', NULL),
(9, 8, 'j\'aime trop ce contact', '2022-10-08 05:42:45', NULL),
(10, 8, 'J\'ai deux comments', '2022-10-08 05:42:45', NULL),
(12, 9, 'Mon premier comment', '2022-10-08 05:44:31', NULL),
(13, 9, 'Mondeuxieme comment', '2022-10-08 05:44:31', NULL),
(14, 10, 'Je valide la ville mon ami', '2022-10-08 07:55:56', NULL),
(17, 12, 'mise a jour du premier comment', '2022-10-08 20:07:22', NULL),
(18, 12, 'Mondeuxieme comment', '2022-10-08 20:07:22', NULL),
(22, 15, 'ZbqhNBvrHlBaHStbxLQhlYUoFjeRvfvKgMphpeGshFNOVMDBSDoWtRwafNttBFvCudmknGIPhkenvvXrc ZQIlezVWHHICyAEgGCeNuXmifesHHjSudfoZs', '2022-10-08 22:19:54', NULL),
(23, 15, 'hellooooooooooooooooooooooooooooooooooooooooooooooooo world', '2022-10-08 22:19:54', NULL),
(24, 17, 'MhALUkTFrarygReBBAA CfTOzUaWxYUxpE', '2022-10-08 22:19:54', NULL),
(29, 4, 'commentaire 1', '2022-10-09 14:09:44', NULL),
(30, 4, 'comment supplémentaire', '2022-10-09 14:09:44', NULL),
(31, 22, 'mise a jour du premier comment', '2022-10-09 15:00:04', NULL),
(32, 18, 'ZbqhNBvrHlBaHStbxLQhlYUoFjeRvfvKgMphpeGshFNOVMDBSDoWtRwafNttBFvCudmknGIPhkenvvXrc ZQIlezVWHHICyAEgGCeNuXmifesHHjSudfoZs', '2022-10-09 15:00:48', NULL),
(33, 18, 'hellooooooooooooooooooooooooooooooooooooooooooooooooo world', '2022-10-09 15:00:48', NULL),
(34, 20, 'MhALUkTFrarygReBBAA CfTOzUaWxYUxpE', '2022-10-09 15:00:48', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `postalcode` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `firstname`, `lastname`, `email`, `telephone`, `city`, `province`, `postalcode`, `country`, `created_on`, `updated_on`) VALUES
(4, 'Algero', 'Fabrice', 'susowijo3@gmail.fr', '(237) 695-5719', 'douala', 'Littoral', 'A9A 9A9', 'Cameroun', '2022-10-08 05:11:52', '2022-10-09 14:09:44'),
(5, 'Alice', 'Gerom', 'susowijo1@gmail.cad', '(237) 695-5719', 'douala', 'Littoral', 'A9A 9A9', 'Cameroun', '2022-10-08 05:14:59', NULL),
(6, 'Angelo', 'Gabila', 'susowijo4@gmail.fr', '(237) 695-5719', 'douala', 'Littoral', 'A9A 9A9', 'Cameroun', '2022-10-08 05:26:19', NULL),
(7, 'Garra', 'Adboul', 'susowijo4@gmail.com', '(237) 695-5719', 'douala', 'Littoral', 'A9A 9A9', 'Cameroun', '2022-10-08 05:32:59', NULL),
(8, 'Beedoso', 'Menshi', 'susowijo1@gmail.net', '(237) 695-5719', 'douala', 'Littoral', 'A9A 9A9', 'Cameroun', '2022-10-08 05:42:45', NULL),
(9, 'Plontozo', 'Murielle', 'susowijo1@gmail.frc', '(237) 695-5719', 'douala', 'Littoral', 'A9A 9A9', 'Cameroun', '2022-10-08 05:44:11', '2022-10-08 05:44:32'),
(10, 'Deffo Boris', 'Kegne Fostin', 'susowijo3@gmail.frd', '(237) 695-5719', 'douala', 'Littoral', 'A9A 9A9', 'Cameroun', '2022-10-08 07:55:56', NULL),
(11, 'Garra', 'Kegne Fostin', 'susowijo3@gmail.frg', '(237) 695-5719', 'douala', 'Littoral', 'A9A 9A9', 'Cameroun', '2022-10-08 07:56:44', NULL),
(12, 'Kevino Debryn', 'SOUKOU BILL', 'sokou@gmail.fr', '(237) 655-4513', 'Yaounde', 'littoral', 'A9A 9A8', 'Cameroun', '2022-10-08 20:06:47', '2022-10-08 20:07:23'),
(13, 'Jordan', 'Suffo Sokamte', 'susowijo9@gmail.com', '(237)655-7845', 'Douala', 'littoral', 'A9A 9A7', 'Cameroun', '2022-10-08 21:30:20', NULL),
(14, 'Jordan', 'Suffo Sokamte', 'susowijo1@gmail.comg', '(819)555-7494', 'Douala', 'littoral', 'A9A 9A7', 'Cameroun', '2022-10-08 21:33:06', NULL),
(15, 'Eolanda', 'Fillbert', 'Eolanda.Fillbert@bizmail.co', '(819)555-7531', 'Edmonton', 'Nb', 'J8X 5T7', 'Central African Republic', '2022-10-08 22:17:28', '2022-10-08 22:19:55'),
(16, 'Diena', 'Lytton', 'Diena.Lytton@bizmail.co', '(613)555-2916', 'Cincinnati', 'Nb', 'J8X 5T7', 'Turks and Caicos Islands', '2022-10-08 22:17:28', '2022-10-08 22:19:55'),
(17, 'Selia', 'Sandye', 'Selia.Sandye@bizmail.co', '(819)555-7494', 'Beirut', 'Qc', 'J9A 8A5', 'Lesotho', '2022-10-08 22:17:28', '2022-10-08 22:19:55'),
(18, 'Eolanda', 'Fillbert', 'Eolanda.Fillbert@bizmail.comm', '(819)555-7531', 'Edmonton', 'Nb', 'J8X 5T7', 'Central African Republic', '2022-10-08 22:24:48', '2022-10-09 15:00:48'),
(19, 'Diena', 'Lytton', 'Diena.Lytton@bizmail.comm', '(613)555-2916', 'Cincinnati', 'Nb', 'J8X 5T7', 'Turks and Caicos Islands', '2022-10-08 22:24:48', '2022-10-09 15:00:48'),
(20, 'Selia', 'Sandye', 'Selia.Sandye@bizmail.comm', '(819)555-7494', 'Beirut', 'Qc', 'J9A 8A5', 'Lesotho', '2022-10-08 22:24:48', '2022-10-09 15:00:48'),
(21, 'Jordan', 'Suffo Sokamte', 'susowijo1@gmail.com', '(237) 255-5454', 'Douala', 'littoral', 'A9A 9A7', 'Cameroun', '2022-10-09 14:57:55', NULL),
(22, 'Jordan', 'Suffo Sokamte', 'kusowijo1@gmail.com', '(234) 546-8954', 'Douala', 'littoral', 'A9A 9A7', 'Cameroun', '2022-10-09 15:00:04', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_comment_contact` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
