﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ContactManagement.Models
{
    /// <summary>
    /// 
    /// </summary>
    public enum ModeType
    {
        /// <summary>
        /// Get not filtered data
        /// </summary>
        [Description("List")]
        List = 1,

        /// <summary>
        /// Get filtered data by search text
        /// </summary>
        [Description("Search")]
        Search = 2
    }
}