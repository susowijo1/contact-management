﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContactManagement.Models
{
    /// <summary>
    /// Class Comment
    /// </summary>
    public class Comment : BaseModel
    {
        /// <summary>
        /// Represents the comment content
        /// </summary>
        [Required, Display(Name = "Commentaire")]
        public string Content { get; set; }

        /// <summary>
        /// Represents the id of the contact concerned by this comment 
        /// </summary>
        [Required]
        public long ContactId { get; set; }

        public Comment(string content, long contactId)
        {
            Content = content;
            ContactId = contactId;
        }

        public Comment(){}
    }
}