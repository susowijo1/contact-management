﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContactManagement.Models
{
    /// <summary>
    /// Class BaseModel
    /// </summary>
    public class BaseModel
    {
        /// <summary>
        /// Represents the id of the entity
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// Represents the creation date of the entity
        /// </summary>
        [Display(Name ="Créer le")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// Represents the modification date of the entity
        /// </summary>
        [Display(Name = "Modifier le")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? UpdatedOn { get; set; }

    }
}