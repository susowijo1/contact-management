﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ContactManagement.Models
{
    /// <summary>
    /// Class Contact:
    /// </summary>
    public class Contact : BaseModel, IValidatableObject
    {
        #region Properties (Public)

        /// <summary>
        /// Represents the first name of the contact
        /// </summary>
        [Required]
        [Display(Name = "Prénom")]
        public string FirstName { get; set; }

        /// <summary>
        /// Represents the last name of the contact
        /// </summary>
        [Required, Display(Name = "Nom")]
        public string LastName { get; set; }

        /// <summary>
        /// Represents the contact email
        /// </summary>
        [Required, EmailAddress]
        [Display(Name = "Adresse e-mail")]
        public string Email { get; set; }

        /// <summary>
        /// Represents the contact phone number
        /// </summary>
        [Required, Display(Name = "Numéro de téléphone")]
        public string Telephone { get; set; }

        /// <summary>
        /// Represents the city of the contact
        /// </summary>
        [Required, Display(Name = "Ville")]
        public string City { get; set; }

        /// <summary>
        /// Represents the province of the contact
        /// </summary>
        [Required]
        public string Province { get; set; }

        /// <summary>
        /// Represents the postal code of the contact
        /// </summary>
        [Required, Display(Name = "Code postal")]
        public string PostalCode { get; set; }

        /// <summary>
        /// Represents the country of the contact
        /// </summary>
        [Required, Display(Name = "Pays")]
        public string Country { get; set; }

        /// <summary>
        /// Represents all comments of the contact
        /// </summary>
        [Display(Name = "Commentaires")]
        public ICollection<Comment> Comments { get; set; }

        /// <summary>
        /// Represents the number of comments of the contact
        /// </summary>
        [NotMapped, Display(Name = "Commentaires")]
        public int? TotalComment { get; set; }

        #endregion

        #region Contructors
        public Contact(string firtName, string lastName, string email, string telephone,
            string city, string province, string postalCode, string country)
        {
            FirstName = firtName;
            LastName = lastName;
            Email = email;
            Telephone = telephone;
            City = city;
            Province = province;
            PostalCode = postalCode;
            Country = country;
        }

        public Contact() { }
        #endregion

        #region Methodes (Public)
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validations = new List<ValidationResult>();

            //Postal Code validation 
            PostalCode = PostalCode.ToUpper();
            var pattern = "[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]";
            var regexPostalCode = new Regex(pattern);
            if (!regexPostalCode.IsMatch(PostalCode))
                validations.Add(new ValidationResult(
                    "Le code postal doit respecter le modèle Canadien: A9A 9A9",
                    new List<string> { "PostalCode" }
                ));

            //Telephone validation 
            pattern = @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$";
            var regexPhoneNumber = new Regex(pattern);
            if (!regexPhoneNumber.IsMatch(Telephone))
                validations.Add(new ValidationResult(
                    "Le numéro de téléphone doit respecter le modèle: (000) 000-0000",
                    new List<string> { "Telephone" }
                ));

            //Email validation 
            var emailParts = Email.Split('.');
            if (emailParts?.Length < 2 || emailParts.LastOrDefault()?.Length < 2)
                validations.Add(new ValidationResult(
                    "L'adresse Email doit se terminer par un nom de domaine. ex: '.com, .fr, .net, ...'",
                    new List<string>{"Email"}
                ));

            return validations;
        }
        #endregion
    }
}